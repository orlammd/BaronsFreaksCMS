----
title: Spectacle
----


{% img('uploads/manytuba/Manytuba_1.png') %}
<h2 class="titraille">Le Manytuba ne répond plus</h2>
<p class="creation-2023">(création 2023)</p>

<div class="show">
<p class="incipit">Ouverture de rideau sur le nouveau bijou de la technomagie moderne : le Jack Caesar Manytubas TriVisio.</p>
<blockquote>
<p>Jack Caesar Manytubas TriVisio : système révolutionnaire de narration augmentée<br/>
(nécessite 3 opérateur·rice·s qualifié·e·s non fourni·e·s)</p>
<cite>MeyerPub Coorp</cite>
{% img('uploads/manytuba/manytubas.png') %}
</blockquote>

<p>Assistée de deux musiciens et d'un stagiaire co-opté,  l’intrigante Mireille Leparquet raconte, images d'archives à l'appui, l'histoire de la transformation de Belleville-la-Neuve en métropole moderne, marketée et connectée. On y fait la rencontre de Jack Caesar, un entrepreneur visionnaire et audacieux, confronté aux Vanupiés, un groupuscule de saltimbanques opposant une résistance futile à la marche du progrès.</p>
<p>Alors qu'elle déroule son récit, une série d'incidents dévie peu à peu le cours de l'histoire. Problème technique ? Sabotage ?</p>
<p>Et si les mystérieux Barons Freaks, fraîchement débarqués à Belleville-La-Neuve, étaient dans le coup ?</p>
<p>Orage au Tournage explose joyeusement les codes du ciné-concert, faisant littéralement déborder l’intrigue sur scène, mêlant le temps du récit à celui d’une improvisation foutraque qui a toutes les chances mener droit au désastre... pour votre plus grand bonheur !</p>
</div>



<div class="photos">
{% img('uploads/manytuba/Manytuba_live_1.png') %}
{% img('uploads/manytuba/Manytuba_live_2.png') %}
  <div class="portrait">
  {% img('uploads/manytuba/jeaneudes_2.jpg') %}
  {% img('uploads/manytuba/saladin_1.jpg') %}
  {% img('uploads/manytuba/doah_1.jpg') %}
  </div>
</div>



### Téléchargements
* [dossier pédagogique]('src/uploads/')
* [pack photos]('src/uploads/')



### Distribution
{% include('inc/distribution.html') %}
___
{% include('inc/production.html') %}
