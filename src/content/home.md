*bizarrerie hybride entre cinéma, théâtre et concert*

### Le Manytuba ne répond plus
{% include('inc/rubrique.html', type='ebook', thumbnail=image_cache('uploads/manytuba/Manytuba_1.png', resize=200), url='spectacle.html', target='_blank', title='"Une sorte d\'histoire avec les Barons Freaks" | Création 2023 | concert / cinéma / théâtre') %}


### Actions culturelles
{% include('inc/rubrique.html', type='ebook', thumbnail=image_cache('uploads/actions-culturelles/2023_EMDSaugonna_1.jpg', resize=200), url='actions culturelles.html', target='_blank', title='Actions culturelles autour du cinéma muet et du ciné-concert') %}


### Distribution
{% include('inc/distribution.html') %}

___
{% include('inc/production.html') %}
