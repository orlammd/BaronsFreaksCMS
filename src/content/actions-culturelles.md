----
title: Actions culturelles
----

{% img('uploads/actions-culturelles/cine-concerts.jpg') %}
<h2 class="titraille">Actions culturelles</h2>
<p class="creation-2023">en marge d'une programmation et/ou à destination des écoles, écoles de musique, IME, ESAT...</p>

<div class="show">
<p class="incipit">Outre leur spectacle, les Barons Freaks proposent des interventions pédago-artistiques auprès de différents publics.</p>
</div>

{% include('inc/actions-culturelles.html', type='ebook', thumbnail=image_cache('uploads/actions-culturelles/2023_EMDSaugonna_2.jpg', resize=200), url='spectacle.html', target='_blank', title='Ciné-concerts', description='Les ateliers ciné-concert, destinés à un public musicien ou non-musicien, sont l’occasion d’envisager la musique comme le prolongement narratif d’une image animée.') %}

{%include('inc/actions-culturelles.html', type='ebook', thumbnail=image_cache('uploads/actions-culturelles/2023_EMDSaugonna_2.jpg', resize=200), url='spectacle.html', target='_blank', title='Tournages participatifs', description="Les tournages participatifs permettent de découvrir la méthode de travail cinématographique des Barons Freaks, en différentes étapes : imagination d’un scénario et de personnages, story-board, repérages préalables, tournage dans les codes du cinéma muet…")  %}

{%include('inc/actions-culturelles.html', type='ebook', thumbnail=image_cache('uploads/actions-culturelles/2023_EMDSaugonna_2.jpg', resize=200), url='spectacle.html', target='_blank', title='Autour des musiques tziganes', description="À travers des extraits musicaux et vidéo, et des morceaux traditionnels joués en duo avec des instruments de différentes aires géographiques couvertes par la culture rom, les Barons Freaks proposent de marquer les jalons du voyage des peuples tziganes depuis le Rajasthan vers l'Europe, lors d'une intervention scolaire prenant la forme d'une exploration musicale documentée et interactive.")  %}
